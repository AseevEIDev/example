package ru.aseevei.example;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import ru.aseevei.example.project.base.BaseModel;
import ru.aseevei.example.project.ui.settings.SettingsFragment;
import ru.aseevei.example.project.ui.settings.SettingsPresenter;
import static junit.framework.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Tests for SettingsPresenter Methods
 */
@RunWith(MockitoJUnitRunner.class)
public class SettingsPresenterTest {

    int count = 10;
    int incrment = 2;
    int max = 100;

    @Mock
    SettingsFragment fragment;
    @Mock
    BaseModel model;
    SettingsPresenter presenter;

    @Before
    public void setUp() throws Exception {
        when(model.getIncrement()).thenReturn(incrment);
        when(model.getMax()).thenReturn(max);

        presenter = new SettingsPresenter(model);
        presenter.bindView(fragment);
    }

    /**
     * Is data loading from Model in Constructor
     */
    @Test
    public void loadDataFromModuleTest() {
        assertEquals(incrment, presenter.getIncrement());
        assertEquals(max, presenter.getMax());
    }

    /**
     * Does Increment change when max less or equal than increment
     * and sent new increment to View in onMax
     */
    @Test
    public void onMaaxLessIncrement() {
        presenter.onMax(2);
        assertEquals(1, presenter.getIncrement());
        verify(fragment).setIncrement(1);
    }

    /**
     * Does presenter send new Count to Model
     */
    @Test
    public void resetCountTest() {
        presenter.onReset();
        verify(model).setCount(0);
    }

    /**
     * Was data sent to View when new View was binded
     */
    @Test
    public void sendDataOnBindTest() {
        verify(fragment).setIncrement(anyInt());
        verify(fragment).setMax(anyInt());
    }

    /**
     * Does data save onDestroy
     */
    @Test
    public void saveDataOnDestroy() {
        presenter.saveData();
        verify(model).setIncrement(anyInt());
        verify(model).setMax(anyInt());
    }
}