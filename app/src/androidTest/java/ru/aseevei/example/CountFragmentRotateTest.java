package ru.aseevei.example;

import android.content.pm.ActivityInfo;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v4.app.Fragment;
import android.widget.TextView;
import static android.support.test.espresso.Espresso.*;
import static android.support.test.espresso.action.ViewActions.*;
import static android.support.test.espresso.matcher.ViewMatchers.*;
import static junit.framework.Assert.*;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import ru.aseevei.example.project.MainActivity;
import ru.aseevei.example.project.ui.count.CountFragment;

/**
 * Tests for rotation activity. Check is data and presenter didn't change after rotate
 */
@RunWith(AndroidJUnit4.class)
public class CountFragmentRotateTest {

    private CountFragment countFragment;
    private TextView tvCount;
    @Rule public ActivityTestRule<MainActivity> mActivityRule =
            new ActivityTestRule<>(MainActivity.class, true, true);

    @Before
    public void setUp() {
        initFragment();
    }

    /**
     * Check if tvCount, Presenter and Data didn't change after rotate
     */
    @Test
    public void chechDataSaveRotate() {
        onView(withId(R.id.tvCount)).perform(click());

        String count = tvCount.getText().toString();
        String presenterLink = countFragment.getPresenter().toString();

        mActivityRule.getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        initFragment();
        assertEquals(count, tvCount.getText());
        assertEquals(presenterLink, countFragment.getPresenter().toString());
        assertEquals(count, String.valueOf(countFragment.getPresenter().getCount()));
    }

    public void initFragment() {
        Fragment fragment = mActivityRule.getActivity().getSupportFragmentManager().findFragmentById(R.id.container);
        assertNotNull(fragment);
        countFragment = (CountFragment) fragment;
        tvCount = countFragment.getView().findViewById(R.id.tvCount);
    }
}