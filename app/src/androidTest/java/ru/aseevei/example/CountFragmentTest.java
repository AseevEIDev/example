package ru.aseevei.example;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v4.app.Fragment;
import android.widget.TextView;
import static android.support.test.espresso.Espresso.*;
import static android.support.test.espresso.action.ViewActions.*;
import static android.support.test.espresso.matcher.ViewMatchers.*;
import static junit.framework.Assert.*;
import static org.junit.Assert.assertNotEquals;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import ru.aseevei.example.project.MainActivity;
import ru.aseevei.example.project.ui.count.CountFragment;

/**
 * CountFragment ui tests
 */
@RunWith(AndroidJUnit4.class)
public class CountFragmentTest {

    private CountFragment countFragment;
    private TextView tvCount;
    @Rule
    public ActivityTestRule<MainActivity> mActivityRule =
            new ActivityTestRule<>(MainActivity.class, true, true);

    @Before
    public void setUp() {
        Fragment fragment = mActivityRule.getActivity().getSupportFragmentManager().findFragmentById(R.id.container);
        assertNotNull(fragment);
        countFragment = (CountFragment) fragment;
        tvCount = countFragment.getView().findViewById(R.id.tvCount);
    }

    /**
     * Check count will change after press
     */
    @Test
    public void checkData() {
        Fragment fragment = mActivityRule.getActivity().getSupportFragmentManager().findFragmentById(R.id.container);
        assertNotNull(fragment);
        countFragment = (CountFragment) fragment;

        tvCount = countFragment.getView().findViewById(R.id.tvCount);
        String count = tvCount.getText().toString();

        onView(withId(R.id.tvCount)).perform(click());

        assertNotEquals(count, tvCount.getText());
    }
}
