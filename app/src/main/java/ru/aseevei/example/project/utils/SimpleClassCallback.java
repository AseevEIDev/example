package ru.aseevei.example.project.utils;

public interface SimpleClassCallback<T> {
    /**
     * Just a simple callback with any data, useful with lambda
     * @param data Any data that will be sent in callback
     */
    void callback(T data);
}
