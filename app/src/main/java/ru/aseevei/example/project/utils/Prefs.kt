package ru.aseevei.example.project.utils

import android.content.SharedPreferences

class Prefs(val preferences: SharedPreferences) {

        fun get(): SharedPreferences {
            return preferences
        }

        fun edit(): SharedPreferences.Editor {
            return preferences.edit()
        }

        fun remove(vararg keys: String) {
            val editor = preferences.edit()
            for (key in keys) {
                editor.remove(key)
            }
            editor.apply()
        }
}