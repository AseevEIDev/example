package ru.aseevei.example.project.mvp;

public interface Presenter {
    void bindView(View view);
    void unbindView();
    void saveData();
}