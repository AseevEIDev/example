package ru.aseevei.example.project.ui.count;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import javax.inject.Inject;
import javax.inject.Named;
import butterknife.BindView;
import butterknife.ButterKnife;
import ru.aseevei.example.R;
import ru.aseevei.example.project.base.BaseFragment;
import ru.aseevei.example.project.di.DaggerApp;
import ru.aseevei.example.project.di.component.CountComponent;
import ru.aseevei.example.project.di.module.CountModule;
import ru.aseevei.example.project.ui.settings.SettingsFragment;
import ru.aseevei.example.project.utils.Utils;

/**
 * Show a count of pressing on the numeral in the middle of the screen.
 * On every press this count will increased on increment value
 * After destroying fragment count will be saved
 * When you get max value - count will reset to 0
 * Increment and max value you can set in settings (Button on Toolbar)
 */
public class CountFragment extends BaseFragment implements CountView {

    CountComponent countComponent;
    @Inject @Named("count") CountPresenter presenter;
    @BindView(R.id.tvCount) TextView tvCount;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.count_fragment, container, false);
        // init views
        setUnbinder(ButterKnife.bind(this, v));
        setCountToolbar();
        // dagger
        countComponent = (CountComponent) DaggerApp.getApp(getContext()).getComponentsHolder()
                .getFragmentComponent(getClass(), new CountModule(this));
        countComponent.inject(this);
        //actions
        tvCount.setOnClickListener(view -> presenter.onCount());
        //presenter
        setPresenter(presenter);
        presenter.bindView(this);

        return v;
    }

    /**
     * Change Title and show Hide Button
     */
    public void setCountToolbar() {
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(false);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        setHasOptionsMenu(true);
        getActivity().setTitle("Count");
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    /**
     * Set opening Settings on click item
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_settings:
                Utils.openFragment(this, new SettingsFragment());
                return true;
            default:
                break;
        }

        return false;
    }

    /**
     * Set text to TextView with Count
     */
    @Override
    public void setText(String text) {
        tvCount.setText(text);
    }

    public CountPresenter getPresenter() {
        return presenter;
    }
}
