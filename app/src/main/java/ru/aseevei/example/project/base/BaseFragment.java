package ru.aseevei.example.project.base;

import android.support.v4.app.Fragment;
import butterknife.Unbinder;
import ru.aseevei.example.project.App;

public class BaseFragment extends Fragment {

    private Unbinder unbinder;
    private BasePresenter presenter;

    /**
     * Set ButterKnife unbinder for unbinding in onDestroy
     * @param unbinder Result of ButterKnife.bind(this, view)
     */
    public void setUnbinder(Unbinder unbinder) {
        this.unbinder = unbinder;
    }

    /**
     * Set presenter for destroy in onDestroy
     */
    public void setPresenter(BasePresenter presenter) {
        this.presenter = presenter;
    }

    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void onPause() {
        super.onPause();
        if (presenter != null) presenter.saveData();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (presenter != null) presenter.unbindView();
        if (unbinder != null) unbinder.unbind();
        if (isRemoving()) {
            App.getApp(getContext()).getComponentsHolder().releaseFragmentComponent(getClass());
        }
    }
}
