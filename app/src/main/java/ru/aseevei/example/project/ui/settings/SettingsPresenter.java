package ru.aseevei.example.project.ui.settings;

import ru.aseevei.example.project.base.BasePresenter;
import ru.aseevei.example.project.mvp.Model;
import ru.aseevei.example.project.mvp.View;

/**
 * SettingsPresenter required Model to get and save values
 * In Constructor presenter takes values from Model and sends it to View
 * View call onIncrement, onMax and onReset methods to save new values
 * On Destroy all values are saving with Model
 */
public class SettingsPresenter extends BasePresenter {

    Model model;
    private int increment;
    private int max;

    public SettingsPresenter(Model model) {
        this.model = model;
        increment = model.getIncrement();
        max = model.getMax();
    }

    /**
     * on Increment change
     */
    void onIncrement(int increment) {
        this.increment = increment;
    }

    /**
     * on max change, check increment < max, else increment = max - 1
     */
    public void onMax(int max) {
        this.max = max;
        if (increment >= max) {
            increment = max - 1;
            if (getView() != null) getView().setIncrement(increment);
        }
    }

    /**
     * reset count value
     */
    public void onReset() {
        model.setCount(0);
    }

    public SettingsView getView() {
        return (SettingsView) super.getView();
    }

    /**
     * Bind View and send data to View
     */
    @Override
    public void bindView(View view) {
        super.bindView(view);
        getView().setIncrement(increment);
        getView().setMax(max);
    }

    /**
     * Save Data to Model
     */
    @Override
    public void saveData() {
        model.setIncrement(increment);
        model.setMax(max);
    }

    public int getMax() {
        return max;
    }

    public int getIncrement() {
        return increment;
    }
}
