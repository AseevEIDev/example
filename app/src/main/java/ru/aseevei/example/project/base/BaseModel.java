package ru.aseevei.example.project.base;

import javax.inject.Inject;
import ru.aseevei.example.project.mvp.Model;
import ru.aseevei.example.project.utils.Prefs;

public class BaseModel implements Model {

    public static final String COUNT = "count";
    public static final String INCREMENT = "increment";
    public static final String MAX = "max";
    Prefs prefs;

    @Inject public BaseModel(Prefs prefs) {
        this.prefs = prefs;
    }

    @Override
    public int getCount() {
        return prefs.get().getInt(COUNT, 0);
    }

    @Override
    public int getIncrement() {
        return prefs.get().getInt(INCREMENT, 1);
    }

    @Override
    public int getMax() {
        return prefs.get().getInt(MAX, 1000);
    }

    @Override
    public void setCount(int count) {
        prefs.edit().putInt(COUNT, count).commit();
    }

    @Override
    public void setIncrement(int increment) {
        prefs.edit().putInt(INCREMENT, increment).commit();
    }

    @Override
    public void setMax(int max) {
        prefs.edit().putInt(MAX, max).commit();
    }
}
