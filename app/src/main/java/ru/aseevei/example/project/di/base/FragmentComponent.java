package ru.aseevei.example.project.di.base;

public interface FragmentComponent<F> {

    void inject(F fragment);
}
