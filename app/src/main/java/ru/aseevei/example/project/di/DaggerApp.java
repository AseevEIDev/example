package ru.aseevei.example.project.di;

import android.app.Application;
import android.content.Context;

/**
 * Init dagger Components Holder
 */
public class DaggerApp extends Application {
    private ComponentsHolder componentsHolder;

    public static DaggerApp getApp(Context context) {
        return (DaggerApp)context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        componentsHolder = new ComponentsHolder(this).init();
    }

    public ComponentsHolder getComponentsHolder() {
        return componentsHolder;
    }
}