package ru.aseevei.example.project.base;

import ru.aseevei.example.project.mvp.Presenter;
import ru.aseevei.example.project.mvp.View;

public class BasePresenter implements Presenter {

    View view;

    @Override
    public void bindView(View view) {
        this.view = view;
    }

    @Override
    public void unbindView() {
        view = null;
    }

    @Override
    public void saveData() {

    }

    public View getView() {
        return view;
    }
}
