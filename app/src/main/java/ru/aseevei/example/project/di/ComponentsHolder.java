package ru.aseevei.example.project.di;

import android.content.Context;
import java.util.HashMap;
import java.util.Map;
import javax.inject.Inject;
import javax.inject.Provider;
import ru.aseevei.example.project.di.base.FragmentComponent;
import ru.aseevei.example.project.di.base.FragmentComponentBuilder;
import ru.aseevei.example.project.di.base.FragmentModule;
import ru.aseevei.example.project.di.component.AppComponent;
import ru.aseevei.example.project.di.component.DaggerAppComponent;
import ru.aseevei.example.project.di.module.AppModule;

/**
 * Creates AppComponent and Subcomponents of Fragments
 */
public class ComponentsHolder {

    private final Context context;
    private AppComponent appComponent;
    @Inject Map<Class<?>, Provider<FragmentComponentBuilder>> builders;
    private Map<Class<?>, FragmentComponent> components;

    public ComponentsHolder(Context context) {
        this.context = context;
    }

    public ComponentsHolder init() {
        appComponent = DaggerAppComponent.builder().appModule(new AppModule(context)).build();
        appComponent.injectComponentsHolder(this);
        components = new HashMap<>();
        return this;
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }

    public FragmentComponent getFragmentComponent(Class<?> cls, FragmentModule module) {
        FragmentComponent component = components.get(cls);
        if (component == null) {
            FragmentComponentBuilder builder = builders.get(cls).get();
            if (module != null)
                builder.module(module);
            component = builder.build();
            components.put(cls, component);
        }
        return component;
    }

    public void releaseFragmentComponent(Class<?> cls) {
        components.put(cls, null);
    }

}
