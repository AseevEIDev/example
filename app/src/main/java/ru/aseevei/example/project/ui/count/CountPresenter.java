package ru.aseevei.example.project.ui.count;

import android.util.Log;

import ru.aseevei.example.project.base.BasePresenter;
import ru.aseevei.example.project.mvp.Model;
import ru.aseevei.example.project.mvp.View;

/**
 * Count presenter require Model to get and save values
 * It increase count on increment when onCount method called
 * If count >= max value - count resets to 0
 * Then presenter sands new count to View
 * OnDestroy presenter save new data with Model class
 */
public class CountPresenter extends BasePresenter {

    private Model model;
    private int count;
    private int increment;
    private int max;

    public CountPresenter(Model model) {
        this.model = model;
        count = model.getCount();
        increment = model.getIncrement();
        max = model.getMax();
        count = count >= max ? 0 : count;
    }

    /**
     * Compute new count - count + increment - and send value to View
     */
    public void onCount() {
        count += increment;
        count = count >= max ? 0 : count;
        if (getView() != null) {
            getView().setText(String.valueOf(count));
        }
    }

    public CountView getView() {
        return (CountView) super.getView();
    }

    /**
     * Bind View and send data to View
     */
    @Override
    public void bindView(View view) {
        super.bindView(view);
        getView().setText(String.valueOf(count));
    }

    /**
     * Save Data to Model
     */
    @Override
    public void saveData() {
        model.setCount(count);
    }

    public int getCount() {
        return count;
    }

    public int getIncrement() {
        return increment;
    }

    public int getMax() {
        return max;
    }
}
