package ru.aseevei.example.project;

import ru.aseevei.example.project.di.DaggerApp;
import ru.aseevei.example.project.utils.Utils;

public class App extends DaggerApp {

    @Override
    public void onCreate() {
        super.onCreate();
        Utils.createSingletones(this);
    }
}
