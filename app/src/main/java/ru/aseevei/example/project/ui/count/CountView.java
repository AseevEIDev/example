package ru.aseevei.example.project.ui.count;

import ru.aseevei.example.project.mvp.View;

public interface CountView extends View{
    public void setText(String text);
}
