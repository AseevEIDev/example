package ru.aseevei.example.project.di.component;

import dagger.Subcomponent;
import ru.aseevei.example.project.base.BaseModel;
import ru.aseevei.example.project.di.annotation.CountContext;
import ru.aseevei.example.project.di.annotation.FragmentContext;
import ru.aseevei.example.project.di.base.FragmentComponent;
import ru.aseevei.example.project.di.base.FragmentComponentBuilder;
import ru.aseevei.example.project.di.module.CountModule;
import ru.aseevei.example.project.ui.count.CountFragment;

/**
 * Count Subcomponent allow to get BaseModel and CountFragment
 */
@FragmentContext
@Subcomponent(modules = CountModule.class)
public interface CountComponent extends FragmentComponent<CountFragment> {

    @Subcomponent.Builder
    interface Builder extends FragmentComponentBuilder<CountComponent, CountModule> {}

    @CountContext CountFragment getCountFragment();
    @FragmentContext BaseModel getBaseModel();
}