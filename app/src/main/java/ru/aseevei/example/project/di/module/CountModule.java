package ru.aseevei.example.project.di.module;

import javax.inject.Named;
import dagger.Module;
import dagger.Provides;
import ru.aseevei.example.project.base.BaseModel;
import ru.aseevei.example.project.di.annotation.CountContext;
import ru.aseevei.example.project.di.annotation.FragmentContext;
import ru.aseevei.example.project.di.base.FragmentModule;
import ru.aseevei.example.project.ui.count.CountFragment;
import ru.aseevei.example.project.ui.count.CountPresenter;

/**
 * Provides CountFragment and CountPresenter
 */
@Module
public class CountModule implements FragmentModule {

    private final CountFragment countFragment;

    public CountModule(CountFragment countFragment) {
        this.countFragment = countFragment;
    }

    @Provides
    @CountContext
    CountFragment provideCountFragment() {
        return countFragment;
    }

    @Provides
    @FragmentContext
    @Named("count")
    CountPresenter provideCountPresenter(BaseModel baseModel) {
        return new CountPresenter(baseModel);
    }
}
