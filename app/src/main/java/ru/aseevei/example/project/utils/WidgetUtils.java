package ru.aseevei.example.project.utils;

import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;
import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar.OnProgressChangeListener;

public class WidgetUtils {

    /**
     * This method returns you ProgressChangeListener with callback for make code shorter
     * @param callback Callback on stop tracking
     * @return ChangeListener with callback
     */
    public static OnProgressChangeListener getSeekListener(SimpleClassCallback<Integer> callback) {
        return new OnProgressChangeListener() {
            @Override
            public void onProgressChanged(DiscreteSeekBar seekBar, int value, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(DiscreteSeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(DiscreteSeekBar seekBar) {
                callback.callback(seekBar.getProgress());
            }
        };
    }
}
