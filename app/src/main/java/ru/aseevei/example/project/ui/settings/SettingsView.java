package ru.aseevei.example.project.ui.settings;

import ru.aseevei.example.project.mvp.View;

public interface SettingsView extends View {
    public void setIncrement(int increment);
    public void setMax(int max);
}
